package ex3;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Counter100 extends Thread {

    String n;
    Thread t;
    Counter100(String n, Thread t){
        this.n = n;
        this.t=t;
    }

    public void run() {
        int i,L;
        System.out.println("Firul "+n+" a intrat in metoda run()");
        try
        {
            if (t!=null) {
                t.join();
                i = 100;
                L = 200;
            }else {
                i = 0;
                L = 100;
            }
            System.out.println("Firul "+n+" executa operatie.");
            Thread.sleep(3000);
            for(i=0;i<=L;i++) {
                System.out.println("Firul "+n+" i = " + i);
            }

            System.out.println("Firul "+n+" a terminat operatia.");
        }
        catch(Exception e){e.printStackTrace();}

    }

    public static void main(String[] args) {
        Counter100 c1 = new Counter100("Proces 1",null);
        Counter100 c2 = new Counter100("Proces 2",c1);
        c1.start();
        c2.start();
    }
}