package ex6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Chronometer extends JFrame {
    private final JLabel timeLabel;
    private final JButton startButton;

    private byte ms = 0;
    private byte s = 30;
    private short m = 0;

    private final Runnable timeTask;
    private final Runnable incrementTimeTask;
    private final Runnable setTimeTask;
    private boolean timerIsRunning = true;

    private final ExecutorService executor = Executors.newCachedThreadPool();

    Chronometer() {
        JPanel panel = new JPanel();
        timeLabel = new JLabel();
        panel.add(timeLabel);

        JPanel buttonPanel = new JPanel();
        startButton = new JButton("Start");
        startButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                if (!timerIsRunning) {
                    startButton.setText("Stop");
                    timerIsRunning = true;
                }
                else {
                    startButton.setText("Start");
                    timerIsRunning = false;
                }
                executor.execute(timeTask);
            }
        });
        buttonPanel.add(startButton);

        JButton resetButton = new JButton("Reset");
        resetButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                timerIsRunning = false;
                ms = 0;
                s = 30;
                m = 0;
                timeLabel.setText(m + ":" + s + "." + ms);
            }
        });
        buttonPanel.add(resetButton);
        panel.add(buttonPanel, BorderLayout.SOUTH);

        timeTask = new Runnable() {
            public void run() {
                while (timerIsRunning) {
                    executor.execute(incrementTimeTask);

                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        };

        incrementTimeTask = new Runnable(){
                public void run()
                {
                    if (ms > 0)
                        ms--;
                    else
                    {
                        if (s == 0 && m == 0)
                            timerIsRunning = false;
                        else if (s > 0)
                        {
                            s--;
                            ms = 99;
                        }
                        else if (m > 0)
                        {
                            m--;
                            s = 59;
                            ms = 99;
                        }
                    }
                    executor.execute(setTimeTask);
                }
            };

        setTimeTask = new Runnable(){
                public void run()
                {
                    timeLabel.setText(m+ ":" + s + "." + ms);
                }
            };

            timeLabel.setText(m + ":" + s + "." + ms );
            add(panel);
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setLocationRelativeTo(null);
            setTitle("Chronometer.java");
            pack();
            setVisible(true);
    }

    public static void main(String[] args) {
        new Chronometer();
    }
}
