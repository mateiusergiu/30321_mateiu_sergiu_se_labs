package ex4;

import java.util.ArrayList;


class TestRobot {
    public static void main(String[] args) {
        int[][] pos = new int[10][2];
        for (int i = 0; i < 10; i++) {
            int a = (int) Math.round(100 * Math.random());
            int b = (int) Math.round(100 * Math.random());
            MoveRobot mr = new MoveRobot(new Robot(i, a, b),pos);
            mr.start();
        }
    }
}

class MoveRobot extends Thread {
    final Robot r;
    final int[][] pos;
    public MoveRobot(Robot r, int[][] pos) {
        this.pos = pos;
        this.r = r;
    }

    public void run() {
        int i = 0;
        int a, b;
        while (++i < 10) {
            synchronized (r) {
                a = r.getX();
                b = r.getY();
                pos[r.getN()][0] = a;
                pos[r.getN()][1] = b;
                int choice = (int) (3 * Math.random() + 1);

                try {
                    switch (choice) {
                        case 1:
                            r.setX(a + 1);
                            break;
                        case 2:
                            r.setX(a - 1);
                            break;
                        case 3:
                            r.setY(b + 1);
                            break;
                        case 4:
                            r.setY(b - 1);
                            break;
                    }
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("The Robot#"+r.getN()+" moves to " + " : [" + r.getX() + "," + r.getY()+ "] " + i);
                    for (int j = 0; j < pos.length; j++) {
                        if (!(r.getN() == j) && r.getX() == pos[j][0] && r.getY() == pos[j][1]) {
                            System.out.println("Robot #" + r.getN() + " collide with Robot #" + j);
                            try {
                                r.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            }
        }
//        for (int j = 0; j < RobotArray.size(); j++) {
//            System.out.println(RobotArray.get(j));
//        }
    }
}

public class Robot extends Thread {
    int n;
    int x;
    int y;

    public Robot(int n, int x, int y) {
        this.n = n;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getN() {
        return n;
    }
}