package ex4;

import java.io.*;

public class IOSystem {
    void CreateFile(String fname) {
        File file = new File("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab8\\src\\ex4\\" + fname + ".TXT");
        boolean result;
        try {
            result = file.createNewFile();
            if (result) {
                System.out.println("File created " + file.getCanonicalPath()); //returns the path string
            } else {
                System.out.println("File already exist at location: " + file.getCanonicalPath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void WriteFile(String fname, String ftext) {
        try (FileWriter fw =
                     new FileWriter("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab8\\src\\ex4\\" + fname + ".TXT",true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out1 = new PrintWriter(bw))
        {
            out1.println(ftext);
        }
       catch (IOException e) {
            System.err.println("An error occurred.");
            e.printStackTrace();
        }
    }

//    try(FileWriter fw = new FileWriter("myfile.txt", true);
//    BufferedWriter bw = new BufferedWriter(fw);
//    PrintWriter out = new PrintWriter(bw))
//    {
//        out.println("the text");
//        //more code
//        out.println("more text");
//        //more code
//    } catch (IOException e) {
//        //exception handling left as an exercise for the reader
//    }


}
