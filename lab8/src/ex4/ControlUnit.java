package ex4;

 class ControlUnit {

     private final static int PresetValue = 32;

     public static int getPresetValue() {
         return PresetValue;
     }

     private ControlUnit(){
     }

     static String NoEvent(){
         return "Nothing to do!";
     }
     static String StartAlarm(){
         return "Alarm Started!";
     }

     static String CallingOwner(){
         return "Calling Owner!";
     }

     static String Cooling(){
        return "Cooling Started!";
     }

     static String Heating(){
         return "Heating Started!";
     }

}
