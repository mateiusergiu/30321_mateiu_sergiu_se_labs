package ex4;



class FireEvent extends Event {

    private static boolean smoke;

    FireEvent(boolean smoke) {
        super(EventType.FIRE);
        this.smoke = smoke;
    }

    static boolean isSmoke() {
        return smoke;
    }

    @Override
    public String toString() {
        return "FireEvent{" + "smoke=" + smoke + '}';
    }

}
