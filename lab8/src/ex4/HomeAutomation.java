package ex4;



import java.util.ArrayList;
import java.util.Random;

public class HomeAutomation {

    public static void main(String[] args){

        String systemFile;
        IOSystem sys = new IOSystem();
        systemFile = "system_logs";
        sys.CreateFile(systemFile);
        Random r1 = new Random();
        Home h = new Home(){
            protected void setValueInEnvironment(Event event){
                System.out.println("New event in environment "+event);
            }
            protected void controllStep(Event event){
                switch (event.getType()) {
                    case TEMPERATURE:
                        if (TemperatureEvent.getValue() < ControlUnit.getPresetValue()) {
                            System.out.println(ControlUnit.Heating());
                            sys.WriteFile(systemFile, ControlUnit.Heating());
                        } else {
                            System.out.println(ControlUnit.Cooling());
                            sys.WriteFile(systemFile, ControlUnit.Cooling());
                        }
                        break;
                    case FIRE:
                        boolean ok = false;
                        ArrayList<FireEvent> fs = new ArrayList<>();
                        FireEvent fe1 = new FireEvent(r1.nextBoolean());
                        FireEvent fe2 = new FireEvent(r1.nextBoolean());
                        FireEvent fe3 = new FireEvent(r1.nextBoolean());
                        fs.add(fe1);fs.add(fe2);fs.add(fe3);
                        for(FireEvent f: fs)
                            if(f.isSmoke()) ok = true;

                        if (ok) {

                            System.out.println(ControlUnit.StartAlarm());
                            System.out.println(ControlUnit.CallingOwner());

                            sys.WriteFile(systemFile, ControlUnit.StartAlarm());
                            sys.WriteFile(systemFile, ControlUnit.CallingOwner());
                        } else {
                            System.out.println(ControlUnit.NoEvent());
                            sys.WriteFile(systemFile, ControlUnit.NoEvent());
                        }
                        break;
                    case NONE:
                        System.out.println(ControlUnit.NoEvent());
                        sys.WriteFile(systemFile, ControlUnit.NoEvent());
                        break;
                    default:
                        System.out.println("Error!");
                        break;
                }
            }
        };
        h.simulate();
    }
}

