package ex3;

import java.util.ArrayList;

class Controler {
    String stationName;
    ArrayList<Controler> cont = new ArrayList<>();
    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();

    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(Controler v) {
             cont.add(v);
    }

    void addControlledSegment(Segment s) {
            list.add(s);
    }

    int getFreeSegmentId() {
        for (Segment s : list) {
            if (s.hasTrain() == false)
                return s.id;
        }
        return -1;
    }

    void controlStep() {
        //check which train must be sent
        int i;
        for (Segment segment : list) {
            if (segment.hasTrain()) {
                Train t = segment.getTrain();
                for(i =0; i<cont.size(); i++){
                if (t.getDestination().equals(cont.get(i).stationName)) {
                    //check if there is a free segment
                    int id = cont.get(i).getFreeSegmentId();
                    if (id == -1) {
                        System.out.println("Trenul +" + t.name + "din gara " + stationName + " nu poate fi trimis catre " + cont.get(i).stationName + ". Nici un segment disponibil!");
                        return;
                    }
                    //send train
                    System.out.println("Trenul " + t.name + " pleaca din gara " + stationName + " spre gara " + cont.get(i).stationName);
                    segment.departTRain();
                    cont.get(i).arriveTrain(t, id);
                }
                }

            }
        }//.for

    }//.

    public void arriveTrain(Train t, int idSegment) {
        for (Segment segment : list) {
            //search id segment and add train on it
            if (segment.id == idSegment)
                if (segment.hasTrain() == true) {
                    System.out.println("CRASH! Train " + t.name + " colided with " + segment.getTrain().name + " on segment " + segment.id + " in station " + stationName);
                    return;
                } else {
                    System.out.println("Train " + t.name + " arrived on segment " + segment.id + " in station " + stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }

        //this should not happen
        System.out.println("Train " + t.name + " cannot be received " + stationName + ". Check controller logic algorithm!");

    }

    public void displayStationState() {
        System.out.println("=== STATION " + stationName + " ===");
        for (Segment s : list) {
            if (s.hasTrain())
                System.out.println("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|");
            else
                System.out.println("|----------ID=" + s.id + "__Train=______ catre ________----------|");
        }
    }
}
