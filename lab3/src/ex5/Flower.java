package ex5;

public class Flower {
        int petal;

        static int count1 = 0;
        {
            count1++;
        }

        Flower(){
            petal = 5;
            //System.out.println("Flower has been created!");
        }
        static int count2 = 0;
        public static int CountFlowers(){
            ++count2;
            return count2;
       }
        public static void main(String[] args) {
            int a=0;
            Flower[] garden = new Flower[5];
            for(int i =0;i<5;i++){
                Flower f = new Flower();
                garden[i] = f;
                a = CountFlowers();
            }

            System.out.println("Using variable: " + Flower.count1);
            //System.out.println("Using method: " + CountFlowers());
            System.out.println("Using method: " + a);
        }
}
