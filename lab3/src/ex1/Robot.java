package ex1;

class Robot{
    public int x;
    Robot(){
        x = 1;
    }
    void Change(int k){
        if(k>=1) x = x + k;
    }

    @Override
    public String toString(){
        return "Robot{" +
                "x=" + x +
                '}';
    }
}
