package ex2;


public class Circle {
    private double radius;
    private String color;
    Circle(){
        radius = 1.0;
        color = "red";
    }

    Circle(int r){
        radius = r;
    }

    Circle(String c){
        color = c;
    }

    public double getRadius(){
        return radius;
    }
    public double getArea(){
        return radius*radius*3.14;
    }
}
