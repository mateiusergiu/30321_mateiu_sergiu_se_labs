package ex3;

public class Author {
    private final String name;
    private String email;
    private final char gender;

    Author(String name,String email, char gender){
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName(){
        return name;
    }

    public  String getEmail(){
        return email;
    }

    public void setEmail(String e){
        email = e;
    }

    public void getGender(){
        if(gender == 'm' || gender == 'f')
            System.out.println(gender);
            //return gender;
        else
            System.out.println("Erorr gender is not m or f");
            //return 'a';
    }

    @Override
    public String toString() {
        return  name + " (" + gender + ") at " + email;
    }
}
