package ex4;

public class TestMyPoint {
    public static void main(String []args){
        MyPoint p = new MyPoint();      //p(0,0)
        System.out.println("Old x:" + p.getX());
        System.out.println("Old y:" + p.getY());
        System.out.println(p);
        System.out.println("Distance: "+ p.distance(4,5));
        p.setX(6);
        p.setY(4);
        System.out.println(p);
        System.out.println("New x:" + p.getX());
        System.out.println("New y:" + p.getY());

        MyPoint p1 = new MyPoint(3,4);  //p1(3,4)
        System.out.println(p1);
        p1.setXY(5,3);
        System.out.println("Distance: "+ p.distance(p1.getX(),p1.getY()));
        System.out.println(p1);
        System.out.println("Distance: "+ p.distance(p1));
        System.out.println(p1);
    }
}
