package ex3;

import java.util.Scanner;
//
public class Prime {
    public static void main(String []args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Input first number: ");
        int num1 = scan.nextInt();
        System.out.println("Input second number: ");
        int num2 = scan.nextInt();
        int mynum, count=0;
        boolean prime = true;
        for(mynum=num1;mynum<=num2;mynum++){
            prime = true;
            for (int i = 2; i <= mynum / 2; i++) {
                if (mynum % i == 0) {
                    prime = false;
                    break;
                }
            }
                if (prime) {
                    System.out.println("Prime number:" + mynum);
                    count++;
                }
            }
        System.out.println("The number of primes is:"+ count);
    }
}
