package ex5;
import java.util.Scanner;
// I wanted to use 2 functions instead of writing everything in main
// I couldn't find a way to print the vector intro a single line
public class SortVector {
    public static int[] GetVector() {
        Scanner scan = new Scanner(System.in);
        int i, N = 10;
        int[] myvector = new int[N];
        for (i = 0; i < N; i++) {
            System.out.println("Input the number for index: " + i);
            myvector[i] = scan.nextInt();
        }
        return myvector;
    }

    public static void Sort(int []vect) {
        int N = vect.length;
        for (int i = 0; i < N - 1; i++)
            for (int j = 0; j < N - i - 1; j++)
                if (vect[j] > vect[j + 1]) {
                    int temp = vect[j];
                    vect[j] = vect[j + 1];
                    vect[j + 1] = temp;
                }
        for (int i = 0; i < N; i++)
            System.out.println(vect[i] + ", ");

    }

    public static void main(String []args) {
        Sort(GetVector());
    }
}
