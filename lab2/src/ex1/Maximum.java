package ex1;

import java.util.Scanner;

public class Maximum {
    public static void main(String []arg){
        Scanner scan = new Scanner(System.in);
        System.out.println("Input first number: ");
        int num1 = scan.nextInt();
        System.out.println("Input second number: ");
        int num2 = scan.nextInt();

        if(num1<num2){
            System.out.println(num2);
        }
        else if (num1>num2){
            System.out.println(num1);
        }
        else System.out.println("Equal!");
    }
}
