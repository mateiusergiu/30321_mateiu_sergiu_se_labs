package ex7;

import java.util.Scanner;

public class NumberGuessingGame { //Ex7
    public static void main(String[] args) {
        int num;
        num = (int) (Math.random() * 99 + 1);
        //System.out.println("Secret number is " + num);
        Scanner scan = new Scanner(System.in);
        int guess;
        int tries = 3;
        do {
            System.out.print("Try your luck: ");
            guess = scan.nextInt();
            if (guess == num)
                System.out.println("You won!");
            else if (guess < num) {
                tries--;
                System.out.println("Wrong answer, your number is too low");
                if(tries!=0) System.out.println("You still have " + tries + " tries left\n");
            }
            else if (guess > num) {
                tries--;
                System.out.println("Wrong answer, your number is too high");
                if(tries!=0) System.out.println("You still have " + tries + " tries left\n");
            }
        } while (guess != num && tries != 0);
        System.out.println("The number was " + num);
        System.out.println("You lost!");
    }
}
