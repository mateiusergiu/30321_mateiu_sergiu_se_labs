package ex4;

import java.util.Scanner;

public class Vector {
    public static void main(String []args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Input the number of elements: ");
        int N = scan.nextInt();
        int i,max;
        int[] myvector = new int[N];
        for(i = 0; i<N; i++){
            System.out.println("Input the number for index: "+i);
            myvector[i] = scan.nextInt();
        }
        max = myvector[0];
        for(i = 0; i<N-1; i++){
            if(myvector[i]>max)
                max = myvector[i];
        }
        System.out.println("Maximum element: "+max);
    }
}
