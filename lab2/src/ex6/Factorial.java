package ex6;
import java.util.Scanner;
public class Factorial {
    public static int NonRecursive(int num){
        if(num>=0) {
            int fact = 1;
            for (int i = 1; i <= num; i++) {
                fact = fact * i;
            }
            return fact;
        }else return -1;
    }

    public static int Recursive(int num) {
        if( num == 0) return 1;
        if (num >= 1)
            return num * Recursive(num - 1);
        else
            return -1;
    }

    public static void main(String []args) {
        int n;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter a integer:");
        n = scan.nextInt();
        System.out.println("Factorial in Non Recursive method:" + NonRecursive(n));
        System.out.println("Factorial in Recursive method:" + Recursive(n));
    }


}
