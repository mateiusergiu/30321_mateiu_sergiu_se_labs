package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CounterTest extends JFrame implements ActionListener {
    JLabel counter;
    JButton bcount;
    JTextField cntr;
    int num = 0;

    CounterTest(){

        setTitle("Test Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        bcount.addActionListener(this);
        setSize(400,250);
        setVisible(true);
    }
    public void init() {
        this.setLayout(null);
        int width=200;int height = 20;

        cntr = new JTextField(2);
        cntr.setBounds(100, 75, width, height);

        //counter = new JLabel("Number: 0 ");
       // counter.setBounds(100, 50, width, height);

        bcount = new JButton("Increase number");
        bcount.setBounds(100,100,width, height);
        add(bcount);// add(counter);
        add(cntr);
    }

    public static void main(String[] args) {
        new CounterTest();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        num  += 1;
        //counter.setText("Number: " + num);
        cntr.setText(Integer.toString(num));
    }

}
