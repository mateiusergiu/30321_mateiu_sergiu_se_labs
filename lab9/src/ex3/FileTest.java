package ex3;

import javax.swing.*;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class FileTest extends JFrame {

    JTextField FileName;
    JTextArea FileContain;
    JButton fButton;

    FileTest() {
        setTitle("Test Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        int width = 200;
        int height = 20;

        FileName = new JTextField(20);
        FileName.setBounds(100, 75, width, height);

        FileContain = new JTextArea();
        FileContain.setBounds(100, 125, width, 4 * height);

        fButton = new JButton("Open File");
        fButton.setBounds(100, 100, width, height);
        fButton.addActionListener(new TestButton());

        add(FileName);
        add(fButton);
        add(FileContain);

        setSize(400, 250);
        setVisible(true);

    }

    void open(String f) {
        try {
            FileContain.setText("");
            BufferedReader bf =
                    new BufferedReader(
                            new FileReader("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab9\\src\\ex3\\" + f));
            String l;
            l = bf.readLine();
            while (l != null) {
                FileContain.append(l + "\n");
                l = bf.readLine();
            }
        } catch (Exception e) {
            System.out.println("File not found!");
        }
    }
    public static void main(String[] args) {
        new FileTest();
    }

    class TestButton implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String fileNameText = FileName.getText();
            open(fileNameText);
        }
    }
}
