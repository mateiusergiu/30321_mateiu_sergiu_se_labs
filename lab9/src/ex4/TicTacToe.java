package ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TicTacToe extends JFrame {
    JLabel player, howToPlay, status;
    JLabel m11, m12, m13, m21, m22, m23, m31, m32, m33;
    JTextField choice;
    JButton refresh;
    String XO = "X", empty = ".";
    Boolean err;

    TicTacToe() {
        setTitle("Tic-Tac-Toe");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 400);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 30;
        int height = 30;
        int pos = 70;

        // POSITIONS:
        {
            m11 = new JLabel(empty);
            m11.setBounds(100, 70, width, height);

            m12 = new JLabel(empty);
            m12.setBounds(100 + pos, 70, width, height);

            m13 = new JLabel(empty);
            m13.setBounds(100 + 2 * pos, 70, width, height);

            m21 = new JLabel(empty);
            m21.setBounds(100, 70 + pos, width, height);

            m22 = new JLabel(empty);
            m22.setBounds(100 + pos, 70 + pos, width, height);

            m23 = new JLabel(empty);
            m23.setBounds(100 + 2 * pos, 70 + pos, width, height);

            m31 = new JLabel(empty);
            m31.setBounds(100, 70 + 2 * pos, width, height);

            m32 = new JLabel(empty);
            m32.setBounds(100 + pos, 70 + 2 * pos, width, height);

            m33 = new JLabel(empty);
            m33.setBounds(100 + 2 * pos, 70 + 2 * pos, width, height);
        }

        refresh = new JButton("Submit");
        refresh.setBounds(200, 20, 4 * width, height);
        refresh.addActionListener(new ButtonSubmit());

        choice = new JTextField();
        choice.setBounds(70, 20, 4 * width, height);

        status = new JLabel("Status");
        status.setBounds(50, 270, 8 * width, height);


        player = new JLabel(XO);
        player.setBounds(50, 20, 4 * width, height);

        howToPlay = new JLabel("Insert in Text Field the position for the X or O.ex:11");
        howToPlay.setBounds(50, 300, 10 * width, height);

        add(m11);add(m12);add(m13);
        add(m21);add(m22);add(m23);
        add(m31);add(m32);add(m33);
        add(refresh);add(choice);add(player);add(howToPlay);add(status);

    }

    class ButtonSubmit implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            err = true;
            int ch = Integer.parseInt(choice.getText());

            switch (ch) {
                case 11:
                    if (m11.getText().equals(empty)) {
                        m11.setText(XO);
                        status.setText("Valid entry");
                    } else {
                        status.setText("You can't add on this position.");
                        err = false;
                    }
                    break;
                case 12:
                    if (m12.getText().equals(empty)) {
                        m12.setText(XO);
                        status.setText("Valid entry");
                    } else {
                        status.setText("You can't add on this position.");
                        err = false;
                    }
                    break;
                case 13:
                    if (m13.getText().equals(empty)) {
                        m13.setText(XO);
                        status.setText("Valid entry");
                    } else {
                        status.setText("You can't add on this position.");
                        err = false;
                    }
                    break;
                case 21:
                    if (m21.getText().equals(empty)) {
                        m21.setText(XO);
                        status.setText("Valid entry");
                    } else {
                        status.setText("You can't add on this position.");
                        err = false;
                    }
                    break;
                case 22:
                    if (m22.getText().equals(empty)) {
                        m22.setText(XO);
                        status.setText("Valid entry");
                    } else {
                        status.setText("You can't add on this position.");
                        err = false;
                    }
                    break;
                case 23:
                    if (m23.getText().equals(empty)) {
                        m23.setText(XO);
                        status.setText("Valid entry");
                    } else {
                        status.setText("You can't add on this position.");
                        err = false;
                    }
                    break;
                case 31:
                    if (m31.getText().equals(empty)) {
                        m31.setText(XO);
                        status.setText("Valid entry");
                    } else {
                        status.setText("You can't add on this position.");
                        err = false;
                    }
                    break;
                case 32:
                    if (m32.getText().equals(empty)) {
                        m32.setText(XO);
                        status.setText("Valid entry");
                    } else {
                        status.setText("You can't add on this position.");
                        err = false;
                    }
                    break;
                case 33:
                    if (m33.getText().equals(empty)) {
                        m33.setText(XO);
                        status.setText("Valid entry");
                    } else {
                        status.setText("You can't add on this position.");
                        err = false;
                    }
                    break;
                default:
                    status.setText("No such position.");
                    break;

            }
            CheckWinner();
            if (err) {
                if (XO.equals("X")) {
                    XO = "O";
                } else {
                    XO = "X";
                }
                player.setText(XO);
                err = false;
            }
        }
    }

    public void CheckWinner() {

        if (m11.getText().equals(m12.getText()) && m12.getText().equals(m13.getText()) && !m12.getText().equals(empty))
            status.setText(XO + " WINS!");
        else if (m21.getText().equals(m22.getText()) && m22.getText().equals(m23.getText()) && !m23.getText().equals(empty))
            status.setText(XO + " WINS!");
        else if (m31.getText().equals(m32.getText()) && m32.getText().equals(m33.getText()) && !m31.getText().equals(empty))
            status.setText(XO + " WINS!");

        else if (m11.getText().equals(m21.getText()) && m21.getText().equals(m31.getText()) && !m11.getText().equals(empty))
            status.setText(XO + " WINS!");
        else if (m12.getText().equals(m22.getText()) && m22.getText().equals(m32.getText()) && !m22.getText().equals(empty))
            status.setText(XO + " WINS!");
        else if (m13.getText().equals(m23.getText()) && m23.getText().equals(m33.getText()) && !m32.getText().equals(empty))
            status.setText(XO + " WINS!");

        else if (m11.getText().equals(m22.getText()) && m22.getText().equals(m33.getText()) && !m33.getText().equals(empty))
            status.setText(XO + " WINS!");
        else if (m13.getText().equals(m22.getText()) && m22.getText().equals(m31.getText()) && !m13.getText().equals(empty))
            status.setText(XO + " WINS!");
    }


    public static void main(String[] args) {
        new TicTacToe();
    }

}
