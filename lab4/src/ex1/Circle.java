package ex1;

//
public class Circle {
    protected int radius;
    protected String color;
    public Circle(){
        radius = 1;
        color = "red";
    }

    public Circle(int radius){
        this.radius = radius;
    }

    public Circle(String color){
        this.color = color;
    }

    public double getRadius(){
        return radius;
    }
    public double getArea(){
        return radius*radius*3.14;
    }
}
