package ex4;

import ex2.Author;


import java.util.Arrays;
//
public class TestBook {
    public static void main(String []args){
        Author[] authors = new Author[3];
        authors[0] = new Author("Mateu Sergiu","s.mateiu@yahoo.com",'m');
        authors[1] = new Author("Mateu Emil","s.mateiu1717@gmail.com",'m');
        authors[2] = new Author("Emil Sergiu","s.mateiu@outlook.com",'m');
        ex4.Book myBook = new ex4.Book("Diff Equations",authors,30);
        ex4.Book myBook2 = new ex4.Book("Improper Integrals",authors,30,100);
        myBook.setQtyInStock(100);
        System.out.println(myBook.getName());
        System.out.println(Arrays.toString(myBook.getAuthors()));
        System.out.println(myBook.getPrice());
        System.out.println(myBook.getQtyInStock());
        myBook.printAuthors();

        System.out.println(myBook2.getName());
        System.out.println(Arrays.toString(myBook2.getAuthors()));
        System.out.println(myBook2.getPrice());
        System.out.println(myBook2.getQtyInStock());
        myBook2.setPrice(60);
        myBook2.printAuthors();

        System.out.println(myBook);
        System.out.println(myBook2.toString());
    }
}
