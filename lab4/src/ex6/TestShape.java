package ex6;
//
public class TestShape {
    public static void main(String[] args) {

        System.out.println("Circles: ");
        Circle c1 = new Circle();
        Circle c2 = new Circle(3);
        Circle c3 = new Circle(7,"red",true);
        c1.setRadius(2);
        System.out.println(c1.getRadius());
        System.out.println(c2.getArea());
        System.out.println(c3.getPerimeter());
        System.out.println(c1.toString());

        System.out.println("Rectangles: ");
        Rectangle r1 = new Rectangle();
        Rectangle r2 = new Rectangle(3,4);
        Rectangle r3 = new Rectangle(5,1,"green",false);
        r2.setFilled(false);
        r1.setWidth(3);
        r2.setLength(8);
        r1.setColor("blue");
        System.out.println(r1.getWidth());
        System.out.println(r2.getLength());
        System.out.println(r3.getArea());
        System.out.println(r3.getPerimeter());
        System.out.println(r3.isFilled());
        System.out.println(r1.getColor());
        System.out.println(r1.toString());

        System.out.println("Squares: ");
        Square s1 = new Square();
        Square s2 = new Square(3);
        Square s3 = new Square(5,"green",false);
        s1.setSide(6);
        System.out.println(s1.getSide());
        s2.setWidth(5);
        System.out.println(s2.getSide());
        s3.setLength(9);
        System.out.println(s3.getSide());
        System.out.println(s2.toString());

    }
}
