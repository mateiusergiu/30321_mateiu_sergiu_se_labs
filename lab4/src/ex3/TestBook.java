package ex3;

import ex2.Author;
//
public class TestBook {
    public static void main(String []args){
        Author myAuthor = new Author("Mateu Sergiu","s.mateiu@yahoo.com",'m');
        Book myBook = new Book("Diff Equations",myAuthor,30);
        Book myBook2 = new Book("Improper Integrals",myAuthor,50,75);
        myBook.setQtyInStock(100);
        System.out.println(myBook.getName());
        System.out.println(myBook.getAuthor());
        System.out.println(myBook.getPrice());
        System.out.println(myBook.getQtyInStock());

        System.out.println(myBook2.getName());
        System.out.println(myBook2.getAuthor());
        System.out.println(myBook2.getPrice());
        System.out.println(myBook2.getQtyInStock());
        myBook2.setPrice(60);

        System.out.println(myBook);
        System.out.println(myBook2.toString());
    }
}
