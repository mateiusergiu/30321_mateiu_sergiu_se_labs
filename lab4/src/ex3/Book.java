package ex3;

import ex2.Author;
//
public class Book {
    private final String name;
    private final Author Author;
    private double price;
    private int qtyInStock;

    public Book(String name, Author author, double price){
        this.name = name;
        this.Author = author;
        this.price = price;
    }
    public Book (String name, Author author, double price,
          int qtyInStock) {
        this.name = name;
        this.Author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }
    public String getName(){
        return name;
    }
    public Author getAuthor(){
        return Author;
    }
    public double getPrice(){
        return price;
    }
    public void setPrice(double price){
        this.price = price;
    }
    public int getQtyInStock() {
        return qtyInStock;
    }
    public void setQtyInStock(int qtyInStock){
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return name + " by " + Author;
    }
}
