package ex5;


//
public class TestCylinder {
    public static void main(String[] args) {
        Cylinder myCylinder1 = new Cylinder();
        Cylinder myCylinder2 = new Cylinder(5);
        Cylinder myCylinder3 = new Cylinder(3,10);
        System.out.println("Cylinder 1");
        System.out.println(myCylinder1.getRadius());
        System.out.println(myCylinder1.getHeight());
        System.out.println(myCylinder1.getArea());
        System.out.println(myCylinder1.getVolume());
        System.out.println("Cylinder 2");
        System.out.println(myCylinder2.getRadius());
        System.out.println(myCylinder2.getHeight());
        System.out.println(myCylinder2.getArea());
        System.out.println(myCylinder2.getVolume());
        System.out.println("Cylinder 3");
        System.out.println(myCylinder3.getRadius());
        System.out.println(myCylinder3.getHeight());
        System.out.println(myCylinder3.getArea());
        System.out.println(myCylinder3.getVolume());



    }
}
