package ex5;


//
public class Cylinder extends ex1.Circle {
 private double height = 1.0;

    public Cylinder(){
        super();
    }
    public Cylinder(double radius){
        super();
        this.radius = (int) radius;
    }
    public Cylinder(double radius, double height) {
        super();
        this.radius = (int) radius;
        this.height = height;
    }

    public double getHeight(){
        return height;
    }

    @Override
    public double getArea() {
        return 2*3.14*radius*height+2*3.14*radius*radius;
    }

    public double getVolume(){
        return super.getArea()*height;
    }



}
