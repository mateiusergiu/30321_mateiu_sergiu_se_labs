package ex4;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class ConsoleMenu {
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public static void main(String[] args) throws Exception {
//        Definition def = new Definition("ex");
//        Word word = new Word("ex");
//        HashMap<Word,Definition> myDict = new HashMap<>();


        Dictionary dict = new Dictionary();
        char response;
        String line;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Menu:");
            System.out.println("a - Add word.");
            System.out.println("s - Search word.");
            System.out.println("w - Get all the words.");
            System.out.println("d - Get all the definitions.");
            System.out.println("e - Exit.");

            line = fluxIn.readLine();
            response = line.charAt(0);

            switch (response) {
                case 'a': case 'A':
                    String explain;
                    System.out.println("Add word:");
                    line= fluxIn.readLine();
                    Word w = new Word(line);
                    if (line.length() > 1) {
                        System.out.println("Add definition:");
                        explain = fluxIn.readLine();
                        Definition d = new Definition(explain);
                        dict.addWord(w, d);
                    }
                    break;
                case 's': case 'S':
                    Definition ex;
                    System.out.println("Word searched:");
                    line= fluxIn.readLine();
                    if (line.length() > 1) {
                        Word word = new Word(line);
                        ex = dict.getDefinition(word);
                        String exp = ex.toString();
                        if (exp == null)
                            System.out.println("It does not exists");
                        else
                            System.out.println("Definition:" + exp);
                    }
                    break;

                case 'w': case 'W':
                    System.out.println("All the Words:");
                    dict.getAllWords();
                    break;


                case 'd': case 'D':
                    System.out.println("All the definitions:");
                    dict.getAllDefinition();
                    break;
            }


        } while (response != 'e' && response != 'E');
        System.out.println("Good bye!.");

    }
}
