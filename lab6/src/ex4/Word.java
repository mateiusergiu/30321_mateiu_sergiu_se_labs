package ex4;

import java.util.Objects;

public class Word {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return name.equals(word.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    private final String name;

    Word(String name){
        this.name = name;
    }
    public String toString() {
        return name;
    }
}
