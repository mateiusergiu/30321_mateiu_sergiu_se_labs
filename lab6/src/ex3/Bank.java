package ex3;
import java.util.Comparator;
import java.util.TreeSet;

public class Bank {
    public static TreeSet<BankAccount> acc;

    Bank(TreeSet<BankAccount> ac){
        this.acc = ac;
    }

    public void addAccount(String owner, double balance) {
        acc.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        for(BankAccount a:acc)
            System.out.println( a + ",");
    }

    public void printSetAccounts(double minBalance,double maxBalance){
        System.out.println(acc.subSet(new BankAccount("X",minBalance),new BankAccount("Y",maxBalance)));
    }

    static class getAllOwners implements Comparator<BankAccount>{
        public int compare(BankAccount b21,BankAccount b22){
            return b21.getOwner().compareTo(b22.getOwner());
        }
    }

    static class getAllBalances implements Comparator<BankAccount>{

        @Override
        public int compare(BankAccount e1, BankAccount e2) {
            if(e1.getBalance() > e2.getBalance()){
                return 1;
            } else {
                return -1;
            }
        }
    }
}

