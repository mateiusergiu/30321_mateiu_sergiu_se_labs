package ex3;

import java.util.TreeSet;

public class TestBank{
    public static void main(String[] args) {
        // You can choose at the beginning which type of treeset to use
        // It is not necessary to copy paste to sort by Balance
        TreeSet<BankAccount> acc = new TreeSet<>(new Bank.getAllOwners());

        Bank myBank=new Bank(acc);
        myBank.addAccount("Sergiu",950);
        myBank.addAccount("Alistar",200);
        myBank.addAccount("Teodora",50);
        myBank.addAccount("Bogdan",400);
        myBank.printAccounts();

        TreeSet<BankAccount> acc2 = new TreeSet<>(new Bank.getAllBalances());
        Bank myBank2=new Bank(acc2);
        myBank2.addAccount("Sergiu",950);
        myBank2.addAccount("Alistar",200);
        myBank2.addAccount("Teodora",50);
        myBank2.addAccount("Bogdan",400);
        myBank2.printAccounts();
        myBank2.printSetAccounts(100,500);
    }
}

