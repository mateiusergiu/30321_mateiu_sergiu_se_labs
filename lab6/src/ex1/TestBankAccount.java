package ex1;

public class TestBankAccount {
    public static void main(String[] args) {
        BankAccount b = new BankAccount("Sergiu",4);
        BankAccount b1 = new BankAccount("Teo",10);

        b.deposit(10);
        b1.withdraw(12);

        System.out.println(b.getBalance());
        System.out.println(b1.getBalance());

        System.out.println(b.equals(b1));
        System.out.println(b.hashCode());
    }

}
