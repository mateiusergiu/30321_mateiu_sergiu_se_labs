package ex2;

import java.util.ArrayList;

public class Bank {
    private ArrayList<BankAccount> accounts;
    Bank(){
        accounts = new ArrayList<>();
    }

    public void addAccount(String owner, double balance){
        accounts.add(new BankAccount(owner, balance));
    }

    public void printAccounts(){
        for (int i=0; i<accounts.size(); i++)
        {
            for (int j=0; j < accounts.size()-j; j++) {
                if (accounts.get(j).getBalance() > accounts.get(j+1).getBalance())
                {
                    BankAccount temp = accounts.get(j);
                    accounts.set(j,accounts.get(j+1) );
                    accounts.set(j+1, temp);
                }
            }
        }
        for (int i=0; i<accounts.size(); i++) {
            System.out.println(accounts.get(i));
        }

    }

    public void printAccounts(double minBalance, double maxBalance){
        for (int i=0; i<accounts.size(); i++) {
            if (accounts.get(i).getBalance() > minBalance && accounts.get(i).getBalance() < maxBalance)
                System.out.println(accounts.get(i));
        }
    }

    public BankAccount getAccount(String owner){
        int i = 0;
        for (i=0; i<accounts.size(); i++) {
            if(accounts.get(i).getOwner().equals(owner)){
                break;
            }
        }
        return accounts.get(i);
    }
    public ArrayList<BankAccount> getAllAccounts() {
        for (int i=0; i<accounts.size(); i++) {
                for (int j=i+1; j < accounts.size(); j++) {
                    if (accounts.get(i).getOwner().compareTo(accounts.get(j).getOwner())>0)
                    {
                        BankAccount temp = accounts.get(i);
                        accounts.set(i,accounts.get(j) );
                        accounts.set(j, temp);
                    }

                }
        }
        return accounts;
        }

    }

