package ex2;

public class TestBank {
    public static void main(String[] args) {
        Bank account = new Bank();
        account.addAccount("Sergiu",100);
        account.addAccount("Teo",70);
        account.addAccount("Bogdan",120);
        account.addAccount("Andrei",50);
        account.addAccount("Pavel",200);
        account.addAccount("Bogdan",120);

//        account.printAccounts();
//        account.printAccounts(80.0,140.0);
        System.out.println(account.getAccount("Sergiu"));
        System.out.println(account.getAllAccounts());
    }
}
