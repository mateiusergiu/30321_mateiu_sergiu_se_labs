package Practice;


public class Puppy {
    int age;

    public Puppy(String name) {
        System.out.println("Name chosen is :" + name);
    }

    public void SetAge(int newAge) {
        age = newAge;
    }

    public int GetAge() {
        System.out.println("Puppy's age is :" + age);
        return age;
    }


    public static void main(String[] args) {
        /* Object creation */
        Puppy myPuppy = new Puppy("tommy");

        /* Call class method to set puppy's age */
        myPuppy.SetAge(2);

        /* Call another class method to get puppy's age */
        myPuppy.GetAge();

        /* You can access instance variable as follows as well */
        System.out.println("Variable Value :" + myPuppy.age);
    }
}
