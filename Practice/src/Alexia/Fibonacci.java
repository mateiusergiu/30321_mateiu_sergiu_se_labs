package Alexia;

import java.util.Scanner;

public class Fibonacci {

    public static void getFib(int n) {
        int[] F = new int[n + 1];
        F[0] = 0;
        F[1] = 1;
        for (int i = 2; i <= n; i++) {
            F[i] = F[i - 1] + F[i - 2];
        }
        System.out.println("The first " + n + " Fibonacci numbers are ");
        for (int i = 1; i < n + 1; i++) {
            System.out.println(F[i]);
        }
    }

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Input a number: ");
        int N = keyboard.nextInt();
        getFib(N);
    }
}
