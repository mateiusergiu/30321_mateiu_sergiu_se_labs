package ex3;

import java.util.Random;

public class LightSensor extends Sensor{
    public Controller lightSensor;

    @Override
    int readValue(){
        Random r = new Random();
        return r.nextInt(100);
    }
}
