package ex3;

public class Controller {
    public LightSensor lightSensor = new LightSensor();
    public TemperatureSensor temperatureSensor = new TemperatureSensor();

    public void control() {
        try {
            for (int i = 0; i < 20; i++) {
                System.out.println(lightSensor.readValue());
                System.out.println(temperatureSensor.readValue());
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Error");
        }
    }
}
