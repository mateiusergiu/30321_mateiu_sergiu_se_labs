package ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor{
    public Controller tempSensor;

    @Override
    int readValue(){
        Random r = new Random();
        return r.nextInt(100);
    }
}
