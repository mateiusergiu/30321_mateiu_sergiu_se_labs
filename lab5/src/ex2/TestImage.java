package ex2;

public class TestImage {
    public static void main(String[] args) {
        RealImage r = new RealImage("example");
        ProxyImage p = new ProxyImage("example1");
        RotatedImage s = new RotatedImage("example2");

        r.display();
        p.display();
        s.display();

        ProxyImage pp = new ProxyImage(s);
        pp.display();
    }

}
