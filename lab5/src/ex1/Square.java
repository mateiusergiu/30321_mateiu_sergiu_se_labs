package ex1;

public class Square extends Rectangle{

    public Square(){
        super();
    }

    public Square(double side ){
        super(side, side);
    }

    public Square(double side,String color, boolean filled){
        super(side,side,color,filled);
    }

    public double getSide(){
        return super.getLength();
    }

    public void setSide(double side){ setLength(side);
        super.setWidth(side);
        super.setLength(side);
    }
   // I supposed that those 2 function are relevant for testing the code
    public double getArea() {
        return length*width;
    }

    public double getPerimeter() {
        return 4*getLength();
    }

    @Override
    public void setWidth(double side){
        super.setWidth(side);
        super.setLength(side);
    }

    @Override
    public void setLength(double side){
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public String toString() {
        return "A Square with side= " + getSide() + ", which is a subclass of " +
                super.toString();
    }

}
