package ex1;

public abstract class Shape {
    protected String color;
    protected boolean filled;

    public Shape(){
        color = "green";
        filled = true;
    }
    public Shape(String color,boolean filled){
        this.color = color;
        this. filled = filled;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    abstract double getArea();

    abstract double getPerimeter();

    @Override
    public String toString() {
        String a;
        if (filled)
            a = "Filled";
        else
            a = "Not filled";
        return "A Shape with color of " + color + " and " + a;
    }

}
