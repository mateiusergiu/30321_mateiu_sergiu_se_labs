package ex1;

public class TestMain {
    public static void main(String[] args) {
        Circle circ = new Circle(5,"red",true);
        Rectangle rect = new Rectangle(5,3,"green",false);
        Square sqre = new Square(7,"blue",true);
        Shape[] shapes = new Shape[3];
        shapes[0] = circ;
        shapes[1] = rect;
        shapes[2] = sqre;
        System.out.println(shapes[0].getArea());
        System.out.println(shapes[0].getPerimeter());

        System.out.println(shapes[1].getArea());
        System.out.println(shapes[1].getPerimeter());

        System.out.println(shapes[2].getArea());
        System.out.println(shapes[2].getPerimeter());
    }

}
