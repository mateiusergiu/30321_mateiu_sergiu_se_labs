package departments;

import entities.Accountant;
import entities.Employee;


import java.util.ArrayList;
import java.util.List;

public class Finances extends Departament{

    private List<Accountant> accountants;

    public Finances(int id, String name) {
        super(id, name);
        accountants = new ArrayList<>();
    }

    public void addEmployee(Employee e){
        accountants.add((Accountant) e);
    }

    public void removeEmployee(int id){
        accountants.remove(id);
    }

    public Employee getEmployee(int id){
        return accountants.get(id);
    }

    public void updateEmployee(Employee e){
        for(Accountant a : accountants){
            if(a.equals(e)){
                if(e.getWorking())
                    e.setWorking(false);
                else
                    e.setWorking(true);
            }else
                System.out.println("The Employee does not exists");
        }
    }

    @Override
    public String toString() {
        return "Finances{" +
                " Id: " + getId() +
                " Name: " + getName() +
                " accountants=" + accountants.toString() +
                '}';
    }
}
