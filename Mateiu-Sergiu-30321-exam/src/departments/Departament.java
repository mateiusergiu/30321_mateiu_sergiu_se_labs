package departments;

import entities.Employee;

import java.util.List;

public class Departament {
    private int id;
    private String name;

    Departament(int id, String name){
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void addEmployee(Employee e){

    }

    public void removeEmployee(int id){

    }

//    public Employee getEmployee(int id){
//
//    }

    public void updateEmployee(Employee e){ }

}
