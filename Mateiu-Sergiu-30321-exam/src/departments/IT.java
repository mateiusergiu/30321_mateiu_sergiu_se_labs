package departments;

import entities.Employee;
import entities.Programmer;

import java.util.ArrayList;
import java.util.List;

public class IT extends Departament{

    private List<Programmer> programmers;

    public IT(int id, String name) {
        super(id, name);
        programmers = new ArrayList<>();
    }

    public void addEmployee(Employee e){
        programmers.add((Programmer) e);
    }

    public void removeEmployee(int id){
        programmers.remove(id);
    }

    public Employee getEmployee(int id){
        return programmers.get(id);
    }

    public void updateEmployee(Employee e){
        for(Programmer p : programmers){
            if(p.equals(e)){
                if(e.getWorking())
                    e.setWorking(false);
                    else
                    e.setWorking(true);
            }else
                System.out.println("The Employee does not exists");
        }
    }
    public String toString() {
        return "IT{" +
                "Id: " + getId() +
                " Name: " + getName() +
                " accountants=" + programmers +
                '}';
    }
}
