package main;


import departments.Finances;
import departments.IT;
import entities.Accountant;
import entities.Company;
import entities.Employee;
import entities.Programmer;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        Finances f = new Finances(1,"FSEGA");
        Accountant a1 = new Accountant(false,1);
        Accountant a2 = new Accountant(false,2);
        Accountant a3 = new Accountant(false,3);
        f.addEmployee(a1);f.addEmployee(a2);f.addEmployee(a3);

        IT it = new IT(2,"NTT");
        Programmer p1 = new Programmer(true,4);
        Programmer p2 = new Programmer(true,5);
        Programmer p3 = new Programmer(true,6);
        it.addEmployee(p1); it.addEmployee(p2); it.addEmployee(p3);

        Company c = new Company("MyCompany","MyAdress");
        //c.initDepartaments();

        c.addDepartment(it); c.addDepartment(f);


        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
        String line;
        char response;
        do {
            System.out.println("Companies:");
            System.out.println(f.toString());
            System.out.println(it.toString());
            System.out.println("0 - Add new Employee");
            System.out.println("1 - Delete Employee from a Department");
            System.out.println("2 - Edit Employee");
            System.out.println("3 - Send Employee to work");
            System.out.println("4 - Send Employee to relax");
            System.out.println("e - Exit.");

            line = fluxIn.readLine();
            response = line.charAt(0);
            String l1, l2;
            char r1,r2;
            switch (response) {
                case '0':
                    System.out.println("Enter a departmaent id: ");
                    l1 = fluxIn.readLine();
                    r1 = line.charAt(0);

                    System.out.println("Enter a departmaent name: ");
                    System.out.println("1 - " + f.getName());
                    System.out.println("2 - " + it.getName());
                    l2 = fluxIn.readLine();
                    r2 = line.charAt(0);

                    switch (r2){
                        case 1:
                            Accountant a = new Accountant(true,r1);
                            System.out.println("Employee added!");
                            break;
                        case 2:
                            Programmer p = new Programmer(true,r1);
                            System.out.println("Employee added!");
                            break;
                    }
                    break;
                case '1':
                    System.out.println("Companies:");
                    System.out.println(f.toString());
                    System.out.println(it.toString());
                    System.out.println("Enter Employee to delete  ");
                    l1 = fluxIn.readLine();
                    r1 = line.charAt(0);

                    switch (r1){
                        case 1:
                            f.removeEmployee(1);
                            System.out.println("Employee deleted!");
                            break;
                        case 2:
                            f.removeEmployee(2);System.out.println("Employee deleted!");
                            break;
                        case 3:
                            f.removeEmployee(3);System.out.println("Employee deleted!");
                            break;
                        case 4:
                            it.removeEmployee(4);System.out.println("Employee deleted!");
                            break;
                        case 5:
                            it.removeEmployee(5);System.out.println("Employee deleted!");
                            break;
                        case 6:
                            it.removeEmployee(6);System.out.println("Employee deleted!");
                            break;
                    }

                    break;
                case '2':

                    break;
                case '3':
                    System.out.println("Companies:");
                    System.out.println(f.toString());
                    System.out.println(it.toString());
                    System.out.println("Enter Employee to put to work  ");
                    l1 = fluxIn.readLine();
                    r1 = line.charAt(0);

                    switch (r1){
                        case 1:
                            a1.setWorking(true);
                            break;
                        case 2:
                            a2.setWorking(true);
                            break;
                        case 3:
                            a3.setWorking(true);
                            break;
                        case 4:
                            p1.setWorking(true);
                            break;
                        case 5:
                            p2.setWorking(true);
                            break;
                        case 6:
                            p3.setWorking(true);
                            break;
                        default:
                            System.out.println("The employee does not exists");
                    }
                    break;
                case '4':
                    System.out.println("Companies:");
                    System.out.println(f.toString());
                    System.out.println(it.toString());
                    System.out.println("Enter Employee to put to work  ");
                    l1 = fluxIn.readLine();
                    r1 = line.charAt(0);

                    switch (r1){
                        case 1:
                            a1.setWorking(false);
                            break;
                        case 2:
                            a2.setWorking(false);
                            break;
                        case 3:
                            a3.setWorking(false);
                            break;
                        case 4:
                            p1.setWorking(false);
                            break;
                        case 5:
                            p2.setWorking(false);
                            break;
                        case 6:
                            p3.setWorking(false);
                            break;
                        default:
                            System.out.println("The employee does not exists");
                    }
                    break;

            }
            try {
                FileWriter out1 = new FileWriter("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\Mateiu-Sergiu-30321-exam\\src\\main\\MyFile");
                StringBuilder sb = new StringBuilder();
                sb.append(f).append(",").append(it);
                out1.write(String.valueOf(sb));
                out1.close();
                System.out.println("Successfully wrote to the file.");
            } catch (IOException e) {
                System.err.println("An error occurred.");
                e.printStackTrace();
            }

        } while (response != 'e' && response != 'E');
        System.out.println("Good bye!");


    }


}


// I knew some classes must be abstract but it was too late to edit
// There must be a faster way to implement but this is what i came up firstly because of lack of time Thank you