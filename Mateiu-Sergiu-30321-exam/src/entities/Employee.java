package entities;

import actions.LunchBreak;
import actions.WorkDay;

public class Employee extends Person implements LunchBreak, WorkDay {
    private static boolean working;
    private int departmentid;

    public Employee(boolean working, int departmentid) {
        this.working = working;
        this.departmentid = departmentid;
    }

    public static Boolean getWorking() {
        return working;
    }

    public static void setWorking(boolean working) {
        Employee.working = working;
    }

    public void work(){
        working = true;
        System.out.println("Working.");
    }

    public void relax(){
        working = false;
        System.out.println("Relaxing.");
    }


}
