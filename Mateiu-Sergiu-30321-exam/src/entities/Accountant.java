package entities;

public class Accountant extends Employee{

    public Accountant(boolean working, int departmentid) {
        super(working, departmentid);
    }

    public void work(){
        Accountant.setWorking(true);
        System.out.println("Accountant Working.");
    }

    public void relax(){
        Accountant.setWorking(false);
        System.out.println("Accountant Relaxing.");
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
