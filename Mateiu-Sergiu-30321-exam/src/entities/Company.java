package entities;

import departments.Departament;
import departments.Finances;
import departments.IT;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private String name;
    private String address;
    private List<Departament> departaments;

    public Company(String name, String address){
        this.name = name;
        this.address = address;
        departaments = new ArrayList<>();
    }

    public void initDepartaments(){
        Finances f = new Finances(1,"FSEGA");
        IT it = new IT(2,"NTT");
    }

    public void addDepartment(Departament d){
        departaments.add(d);
    }


}
