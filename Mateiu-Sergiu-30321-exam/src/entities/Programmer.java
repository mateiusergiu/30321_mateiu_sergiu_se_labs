package entities;

import entities.Employee;

public class Programmer extends Employee {

    public Programmer(boolean working, int departmentid) {
        super(working, departmentid);
    }

    public void work(){
        Programmer.setWorking(true);
        System.out.println("Programmer Working.");
    }

    public void relax(){
        Programmer.setWorking(false);
        System.out.println("Programmer Relaxing.");
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
