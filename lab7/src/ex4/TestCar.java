package ex4;

import java.io.IOException;

public class TestCar {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        CarFactory f = new CarFactory();

        Car c1 = f.buildCar("Tesla",100.000);
        Car c2 = f.buildCar("Dacia",1.000);
         Car c3 = f.buildCar("Skoda",70.000);

        f.storeCar(c1,"CarStore.txt");
        f.storeCar(c2,"CarStore.txt");
        f.storeCar(c3,"CarStore.txt");

        Car x = f.getCar("CarStore.txt");
        System.out.println(x);

    }
}
