package ex4;


import java.io.*;

public class CarFactory {
    Car buildCar(String name,double price){
        Car c = new Car(name,price);
        System.out.println(c+" is ready to be sold.");
        return c;
    }
    void storeCar(Car c, String storeName) throws IOException {
        ObjectOutputStream o =
                new ObjectOutputStream(
                        new FileOutputStream(storeName));

        o.writeObject(c);
        System.out.println(c + ":Up for sale.");
    }

    Car getCar(String storeName) throws IOException, ClassNotFoundException {
        ObjectInputStream in =
                new ObjectInputStream(
                        new FileInputStream(storeName));
        Car x = (Car) in.readObject();
        System.out.println(x+":Car from store.");
        return x;
    }


}
