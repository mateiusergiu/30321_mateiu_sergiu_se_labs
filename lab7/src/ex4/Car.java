package ex4;

import java.io.Serializable;

public class Car implements Serializable {
    public String model;
    public double price;

    public Car(String model, double price){
        this.model = model;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }
}
