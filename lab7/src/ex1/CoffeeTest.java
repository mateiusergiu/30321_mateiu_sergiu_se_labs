package ex1;

public class CoffeeTest {
    public static void main(String[] args) {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();
        int n = 20;
        int count = 0;
        for (int i = 0; i < n; i++) {
            Coffee c = mk.makeCoffee();
            count++;
            try {
                d.drinkCofee(c);
                mk.numberOfCoffees(count);
            } catch (TemperatureException e) {
                System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
            } catch (CoffeeNumberException e){
                System.out.println("Exception:" + e.getMessage() + " numb=" + e.getNumber());
                break;
            } finally {
                System.out.println("Throw the coffee cup.\n");
            }
        }
    }
}
