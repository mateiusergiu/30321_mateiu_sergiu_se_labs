package ex1;

public class CoffeeMaker {

    void numberOfCoffees(int n) throws CoffeeNumberException{
        if(n >= 15)
            throw new CoffeeNumberException(n,"The number of defined coffees are done!");
    }

    Coffee makeCoffee(){
        System.out.println("Make a coffee");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Coffee cofee = new Coffee(t,c);
        return cofee;
    }


}
