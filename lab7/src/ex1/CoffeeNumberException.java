package ex1;

public class CoffeeNumberException extends Throwable {
    int n;

    public CoffeeNumberException(int n, String msg){
        super(msg);
        this.n =n;
    }

    int getNumber(){
        return n;
    }

}
