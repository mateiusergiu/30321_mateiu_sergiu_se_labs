package ex1;

public class CoffeeDrinker {
    void drinkCofee(Coffee c) throws TemperatureException, ConcentrationException{
        if(c.getTemp()>90)
            throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>90)
            throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");
        System.out.println("Drink cofee:"+c);
    }
}
