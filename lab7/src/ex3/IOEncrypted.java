package ex3;

import java.io.*;

public class IOEncrypted {
    void CreateFile(String fname) throws IOException {
        File file = new File("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab7\\src\\ex3\\" + fname + ".TXT");
        boolean result;
        try {
            result = file.createNewFile();
            if (result) {
                System.out.println("File created " + file.getCanonicalPath()); //returns the path string
            } else {
                System.out.println("File already exist at location: " + file.getCanonicalPath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void EncrypteFile(String fname) {
        File encFile = new File("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab7\\src\\ex3\\" + fname + ".ENC");
        boolean result;
        try {
            result = encFile.createNewFile();
            if (result) {
                System.out.println("Encrypted file created " + encFile.getCanonicalPath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {

            BufferedInputStream in = new BufferedInputStream(
                    new FileInputStream("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab7\\src\\ex3\\" + fname + ".TXT"));
            PrintStream out1 = new PrintStream(new BufferedOutputStream(
                    new FileOutputStream("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab7\\src\\ex3\\" + fname + ".ENC")));
            System.setIn(in);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(System.in));
            String s;
            char[] word;
            int i;
            while ((s = br.readLine()) != null) {
                word = s.toCharArray();
                for (i = 0; i < s.length(); i++)
                   out1.print((char) (word[i] << 1));
            }
            out1.close();
            System.out.println("Encrypted!");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void DecrypteFile(String fname) {
        File encFile = new File("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab7\\src\\ex3\\" + fname + ".DEC");
        boolean result;
        try {
            result = encFile.createNewFile();
            if (result) {
                System.out.println("Decrypted file created " + encFile.getCanonicalPath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedInputStream in = new BufferedInputStream(
                    new FileInputStream("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab7\\src\\ex3\\" + fname + ".ENC"));
            PrintStream out1 = new PrintStream(new BufferedOutputStream(
                    new FileOutputStream("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab7\\src\\ex3\\" + fname + ".DEC")));
            System.setIn(in);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(System.in));
            String s;
            char[] word;
            int i;
            while ((s = br.readLine()) != null) {
                word = s.toCharArray();
                for (i = 0; i < s.length(); i++)
                    out1.print((char) (word[i] >> 1));
            }
            out1.close();
            System.out.println("Decrypted!");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void WriteFile(String fname, String ftext) throws IOException {
        try {
            FileWriter out1 =
                    new FileWriter("C:\\School\\Year2s2\\SE\\30321_mateiu_sergiu_se_labs\\lab7\\src\\ex3\\" + fname + ".TXT");
            out1.write(ftext);
            out1.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.err.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
