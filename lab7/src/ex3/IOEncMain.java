package ex3;

import java.io.*;

public class IOEncMain {
    public static void main(String[] args) throws IOException {

        IOEncrypted ioe = new IOEncrypted();
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));


        String line;
        char response;

        String fileName = "";
        System.out.println("Give a name for your file:");
        fileName = fluxIn.readLine();
        do {
            System.out.println("Menu:");
            System.out.println("a - Create File.");
            System.out.println("w - Write in your File.");
            System.out.println("n - Encrypte File.");
            System.out.println("d - Decrypte File.");
            System.out.println("e - Exit.");

            line = fluxIn.readLine();
            response = line.charAt(0);


            switch (response) {
                case 'a': case 'A':
                    ioe.CreateFile(fileName);
                    break;
                case 'w': case 'W':
                    System.out.println("Write text down:");
                    String text = fluxIn.readLine();
                    ioe.WriteFile(fileName,text);
                    break;
                case 'n': case 'N':
                    ioe.EncrypteFile(fileName);
                    break;
                case 'd': case 'D':
                    ioe.DecrypteFile(fileName);
                    break;
            }

        } while (response != 'e' && response != 'E');
        System.out.println("Good bye!");

    }
}





